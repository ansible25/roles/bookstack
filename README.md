# BookStack

A role that installs BookStack

## Table of content

* [Default Variables](#default-variables)
  * [app_url](#app_url)
  * [bookstackpass](#bookstackpass)
  * [deployment_method](#deployment_method)
  * [hostname](#hostname)
  * [mysql_root_password](#mysql_root_password)
  * [php_ver](#php_ver)
  * [proto](#proto)
* [Dependencies](#dependencies)
* [License](#license)
* [Author](#author)

---

## Default Variables

### app_url

#### Default value

```YAML
app_url: '{{ proto }}{{ hostname }}'
```

### bookstackpass

#### Default value

```YAML
bookstackpass: secret
```

### deployment_method

#### Default value

```YAML
deployment_method: local_db
```

### hostname

#### Default value

```YAML
hostname: bookstack.local
```

### mysql_root_password

#### Default value

```YAML
mysql_root_password: rootpass
```

### php_ver

#### Default value

```YAML
php_ver: 74
```

### proto

#### Default value

```YAML
proto: http://
```

## Dependencies

* ansible.posix
* community.general

## License

['MIT']

## Author

bradthebuilder
